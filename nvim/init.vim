" Some basics:
	set title
	set clipboard+=unnamedplus
	set nocompatible
	:set tabstop=4
	:set shiftwidth=4
	syntax on
	set encoding=utf-8
	set number relativenumber
	set bg=dark
	set mouse=a
	set noruler
	set laststatus=0
	set noshowcmd
	set noshowmode
	set hlsearch
	nnoremap <F3> :set hlsearch!<CR>

" Shortcutting split navigation, saving a keypress:
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Replace all is aliased to S.
	nnoremap S :%s//g<Left><Left>

" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Automatically deletes all trailing whitespace and newlines at end of file on save.
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritePre * %s/\n\+\%$//e
	autocmd BufWritePre *.[ch] %s/\%$/\r/e
